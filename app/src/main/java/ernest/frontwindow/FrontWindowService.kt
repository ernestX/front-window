package ernest.frontwindow

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.view.WindowManager
import android.widget.TextView
import androidx.core.content.getSystemService

class FrontWindowService : Service() {
    var windowManager: WindowManager? = null
    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        windowManager = getSystemService()

        windowManager?.let {

            val params = null
            it.addView(TextView(this).also { it.text = "hello" },params)
        }

    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        return super.onStartCommand(intent, flags, startId)
    }
}